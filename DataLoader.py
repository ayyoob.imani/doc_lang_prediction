__author__ = 'vandermonde'
import numpy as np

def load_data(data_dir):
    x_train = get_doc_vectors(data_dir+"/x_train.txt")
    x_test = get_doc_vectors(data_dir+"/x_test.txt")
    y_train = get_target_vectors(data_dir+"/y_train.txt")
    y_test = get_target_vectors(data_dir+"/y_test.txt")

    return x_train, x_test, y_train, y_test


def get_doc_vectors(file):
    pass


def get_target_vectors(file):
    langs = open(file, 'r')
    data = langs.readlines()

    language_index = setup_language_index(data)
    return create_target_vectors(data, language_index)


def setup_language_index(data):
    lang_index = {}
    seen_langs = set()

    index = 0
    for lang in data:
        if lang not in seen_langs:
            seen_langs.add(lang)
            lang_index[lang] = index
            index += 1

    print lang_index
    return lang_index


def create_target_vectors(data, language_index):
    res = np.zeros(len(data), len(language_index))
    for i, lang in enumerate(data):
        res[i, language_index[lang]] = 1

    return res
