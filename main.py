__author__ = 'vandermonde'

from DataLoader import load_data

data_dir = "./data"

x_train, x_test, y_train, y_test = load_data(data_dir)